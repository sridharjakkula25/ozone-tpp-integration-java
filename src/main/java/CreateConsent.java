
import org.json.JSONObject;

import com.jayway.jsonpath.JsonPath;

import constants.SandBoxConstants;
import pojo.AccessToken;
import utility.OzoneUtility;
import utility.TokenUtility;

public class CreateConsent {
			
	public static void main(String[] args) {
		String clientID = SandBoxConstants.constants.getProperty("client_id");
		String clientSecret = SandBoxConstants.constants.getProperty("client_secret");
		
		System.out.println(clientID);
		System.out.println(clientSecret);
		
		//Get Bearer Access Token
		AccessToken accessToken = OzoneUtility.getBearerToken(clientID, clientSecret);
		System.out.println("************* Bearer Acess Token ****************");
		System.out.println(accessToken.getAccess_token());
		
		//Create Consent
		String financialID = SandBoxConstants.constants.getProperty("financialid");
		String response = OzoneUtility.postAccountRequest(accessToken.getAccess_token(), financialID);
		
		System.out.println("************** Consent reponse ******************");
		System.out.println(response);
		
		JSONObject jsonObject = new JSONObject(response);
		
		
		String consentId = (String) new JSONObject(jsonObject.get("Data").toString()).get("AccountRequestId");
		
		System.out.println("************** Consent ID ******************");
		System.out.println(consentId);
		
		String redirectUri = SandBoxConstants.constants.getProperty("redirectUri");
		String aud = SandBoxConstants.constants.getProperty("ozone-aud");
		
		String preUrlJwt = TokenUtility.getPreUrlJwt(clientID, consentId, redirectUri, aud);
		
		System.out.println("************** Preurl JWT ******************");
		System.out.println(preUrlJwt);
		//Consent Authorisation
		String preJwtredirectUri = "https://app.getpostman.com/oauth2/callback";
		String urlEndPoint = "https://modelobankauth2018.o3bank.co.uk:4101/auth";
		String responseType = "code+id_token";
		String scope = "accounts openid";
		String preAuthUrl = urlEndPoint + "?client_id="+clientID 
			     + "&redirect_uri="+preJwtredirectUri 
			     + "&response_type="+responseType
			     + "&scope="+"openid%20accounts"
			     + "&request="+preUrlJwt;
		
		System.out.println("************** Pre-Auth URL ******************");
		System.out.println(preAuthUrl);
	}
	
	
	

}
