import java.util.ArrayList;
import java.util.List;


import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.nimbusds.jwt.JWTClaimsSet;

import constants.SandBoxConstants;
import pojo.AccessToken;
import utility.HTTPSClient;
import utility.TokenUtility;

public class DynamicRegistration {
	// TODO Auto-generated method stub
	
	public static void main(String[] args) {
	
		String softwareStatementId = SandBoxConstants.constants.getProperty("software_statement_id");
		System.out.println("Preparing jwt token for directory "
				+ "sandbox access token for software statment: " + softwareStatementId);
		
		// Generate jwt for directory sandbox access token
		JWTClaimsSet jwtClaimsSet = TokenUtility.getSandBoxClaims();						
		String sandbox_jwt = TokenUtility.getSignJwt(softwareStatementId, jwtClaimsSet);		
		System.out.println("\n*****************Directory Sandbox JWT **************\n");
		System.out.println(sandbox_jwt);
		
		/* Get Directory SandBox Access Token  */		
		AccessToken accessToken = TokenUtility.getSandboxAccessToken(softwareStatementId, sandbox_jwt);	
		String token = accessToken.getAccess_token();
		System.out.println("\n***** Sandbox AccessToken *****\n");
		System.out.println(token);
		
		/* Get SSA from Directory Sandbox */		
  		String organisationID = SandBoxConstants.constants.getProperty("organisationId");
		String ssaToken = TokenUtility.getSSA(token, softwareStatementId,  organisationID);				
		System.out.println("***************** SSA  **************\n");
		System.out.println(ssaToken);
		
		/** Generate JWT for dynamic registration **/
		JWTClaimsSet jwtClaims = TokenUtility.getDynamicRegistrationClaims(ssaToken);		
		String signedJwt = TokenUtility.getSignJwt(SandBoxConstants.constants.getProperty("software_statement_id"), jwtClaims);
		System.out.println("************* Dynamic Registration JWT **************");
		System.out.println(signedJwt);
		
		
		
		/*** Register client ***/
		registerClient(signedJwt);	
		
		
	}
	
	private static void registerClient(String signedJwt) {
		
		String ozoneBaseUrl = SandBoxConstants.constants.getProperty("oZoneBaseUrl");
		String regPath = SandBoxConstants.constants.getProperty("oZoneRegistrationPath");				
		String ozoneRegistrationPath = ozoneBaseUrl + "/" + regPath;
		
		BasicHeader header = new BasicHeader("Content-Type", "application/jwt");
		List<BasicHeader> headers = new ArrayList<BasicHeader>();
		headers.add(header);
		
		String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
	    String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
	    String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
	    String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
	    String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
		
	    CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
				transportCertAlias, idStorePassword, trasportStorePassWord);
	    
	    String dynamicRegResponse = HTTPSClient.callPostMethod(client, ozoneRegistrationPath, headers, signedJwt);
	    
	    if (dynamicRegResponse != null && dynamicRegResponse.length() != 0) {
	    	System.out.println("************* Dynamic Registration Reponse **************");
	    	System.out.println(dynamicRegResponse);
	    	
	    	Gson gson = new GsonBuilder().setPrettyPrinting().create();
	    	JsonParser jp = new JsonParser();
			JsonElement je = jp.parse(dynamicRegResponse);
			String prettyJsonString = gson.toJson(je);
			System.out.println(prettyJsonString);
			
	    }
	    
	    JSONObject jsonObject = new JSONObject(dynamicRegResponse);
	    String clientID = jsonObject.get("client_id").toString();
	    String clientSecret = jsonObject.get("client_secret").toString();
	    
	    SandBoxConstants.writeIntoProperties("client_id", clientID);
	    SandBoxConstants.writeIntoProperties("client_secret", clientSecret);
		
	}
	
}


