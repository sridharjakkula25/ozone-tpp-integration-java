import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import com.fasterxml.jackson.databind.ObjectMapper;

import constants.SandBoxConstants;
import pojo.AccessToken;
import pojo.GrantAccessToken;
import utility.HTTPSClient;
import utility.TokenUtility;

public class ExchangeCode {	
	
	public static void main(String[] args) {
		String clientID = SandBoxConstants.constants.getProperty("client_id");
		String clientSecret = SandBoxConstants.constants.getProperty("client_secret");
		String code = SandBoxConstants.constants.getProperty("code");
		
		System.out.println(code);
		
		String basicAuth = TokenUtility.getBasicAuthToken(clientID, clientSecret);
		
		
		List<BasicHeader> headers = new ArrayList<BasicHeader>();
		headers.add(new BasicHeader("Content-Type", "application/x-www-form-urlencoded"));
		headers.add(new BasicHeader("Authorization", "Basic "+basicAuth));
		
		List<NameValuePair> form = new ArrayList<NameValuePair>();
		form.add(new BasicNameValuePair("grant_type", "authorization_code"));
		form.add(new BasicNameValuePair("scope", "accounts"));
		form.add(new BasicNameValuePair("code", code));
		form.add(new BasicNameValuePair("redirect_uri", "https://app.getpostman.com/oauth2/callback"));
		
		String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
	    String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
	    String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
	    String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
	    String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
		
		
		CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
				transportCertAlias, idStorePassword, trasportStorePassWord);
		
		
		String exchangeCodeEndPoint = SandBoxConstants.constants.getProperty("exchangeCodeEndPoint");
		
		String response = HTTPSClient.callPostMethod(client, exchangeCodeEndPoint, headers, form);
		
		
		System.out.println(response);
		
		GrantAccessToken accessToken;

		if (response != null && response.contains("access_token")) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				accessToken = mapper.readValue(response, GrantAccessToken.class);
				System.out.println("Access Token: " + accessToken.getAccess_token());
				SandBoxConstants.writeIntoProperties("resource_access_token", accessToken.getAccess_token());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

}
