import java.util.ArrayList;
import java.util.List;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;

import constants.SandBoxConstants;
import utility.HTTPSClient;


public class AccessAPI {
	
	
	
	
	public static void main(String[] args) {
		
		String accessToken = SandBoxConstants.constants.getProperty("resource_access_token");
		
		System.out.println("***************** Bulk Accunts *****************");
		String accounts = getAccounts(accessToken);
		System.out.println(accounts);
		
		System.out.println("***************** Bulk balances *****************");
		String balances = getBalances(accessToken);
		System.out.println(balances);
		
		System.out.println("***************** Bulk benificiaries *****************");
		String benificiaries = getBenificiaries(accessToken);
		System.out.println(benificiaries);
		
		System.out.println("***************** Bulk direct debits *****************");
		String directDebits = getBenificiaries(accessToken);
		System.out.println(directDebits);
		
	}
	
	private static String getAccounts(String accessToken) {
		String response = null;		
		List<BasicHeader> headers = getHeaders(accessToken);

		CloseableHttpClient client = getHttpClient();
		
		String accountsEndPoint = SandBoxConstants.constants.getProperty("oZoneBaseUrl")
								  + SandBoxConstants.constants.getProperty("accountsEndPoint") ;
		
		response = HTTPSClient.callGetMethod(client, accountsEndPoint, headers);
		
		return response;
		
	}
	
	private static String getDirectDebits(String accessToken) {
		String response = null;		
		List<BasicHeader> headers = getHeaders(accessToken);

		CloseableHttpClient client = getHttpClient();
		
		String accountsEndPoint = SandBoxConstants.constants.getProperty("oZoneBaseUrl")
								  + SandBoxConstants.constants.getProperty("directDebits") ;
		
		response = HTTPSClient.callGetMethod(client, accountsEndPoint, headers);
		
		return response;
		
	}
	
	private static String getBalances(String accessToken) {
		String response = null;		
		List<BasicHeader> headers = getHeaders(accessToken);

		CloseableHttpClient client = getHttpClient();
		
		String accountsEndPoint = SandBoxConstants.constants.getProperty("oZoneBaseUrl")
								  + SandBoxConstants.constants.getProperty("balanceEndPoint") ;
		
		response = HTTPSClient.callGetMethod(client, accountsEndPoint, headers);
		
		return response;
	}
	
	private static String getBenificiaries(String accessToken) {
		String response = null;		
		List<BasicHeader> headers = getHeaders(accessToken);

		CloseableHttpClient client = getHttpClient();
		
		String accountsEndPoint = SandBoxConstants.constants.getProperty("oZoneBaseUrl")
								  + SandBoxConstants.constants.getProperty("beneficiaries") ;
		
		response = HTTPSClient.callGetMethod(client, accountsEndPoint, headers);
		
		return response;
	}
	
	private static CloseableHttpClient getHttpClient() {
		String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
	    String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
	    String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
	    String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
	    String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
		
		
		CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
				transportCertAlias, idStorePassword, trasportStorePassWord);
		
		return client;
	}
	
	private static List<BasicHeader> getHeaders(String accessToken) {
		List<BasicHeader> headers = new ArrayList<BasicHeader>();
		headers.add(new BasicHeader("Content-Type", "application/json"));
		headers.add(new BasicHeader("Authorization", "Bearer "+accessToken));
		headers.add(new BasicHeader("x-fapi-financial-id", "0015800001041RHAAY"));
		headers.add(new BasicHeader("x-fapi-interaction-id", "4529da82-cf86-48f1-a2b2-f2b4d3055903"));		
		return headers;
	}

}
