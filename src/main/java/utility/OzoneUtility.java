package utility;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import com.fasterxml.jackson.databind.ObjectMapper;

import utility.HTTPSClient;
import constants.SandBoxConstants;
import pojo.AccessToken;




public class OzoneUtility {
	
	public static AccessToken getBearerToken(String client_id, String client_secerete) {
		
		    AccessToken accessToken = null;
		    String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
		    String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
		    String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
		    String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
		    String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
			
			
			CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
					transportCertAlias, idStorePassword, trasportStorePassWord);
			
			Base64 b = new Base64();
            String encoding = b.encodeAsString(new String(client_id+":"+client_secerete).getBytes());
			
			List<BasicHeader> headers = new ArrayList<BasicHeader>();
			headers.add(new BasicHeader("Content-Type", "application/x-www-form-urlencoded"));
			headers.add(new BasicHeader("Authorization", "Basic "+encoding));
			
			List<NameValuePair> form = new ArrayList<NameValuePair>();
			
			form.add(new BasicNameValuePair("grant_type", "client_credentials")) ;
			form.add(new BasicNameValuePair("scope", "accounts openid"));
			
			String ozoneTokenEndPOint = SandBoxConstants.constants.getProperty("ozoneTokenEndPoint");
			
			
			
			String response = HTTPSClient.callPostMethod(client, ozoneTokenEndPOint, headers, form);
			
			if (response != null && response.contains("access_token")) {
				ObjectMapper mapper = new ObjectMapper();
				try {
					accessToken = mapper.readValue(response, AccessToken.class);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		   return accessToken;
				
		
	}
	
	
	public static String postAccountRequest(String accessToken, String financialId) {
		String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
	    String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
	    String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
	    String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
	    String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
		
		
		CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
				transportCertAlias, idStorePassword, trasportStorePassWord);
		
		List<BasicHeader> headers = new ArrayList<BasicHeader>();
		headers.add(new BasicHeader("Content-Type", "application/json"));
		headers.add(new BasicHeader("x-fapi-financial-id", financialId));
		headers.add(new BasicHeader("x-fapi-interaction-id", "34567"));
		headers.add(new BasicHeader("Authorization", "Bearer "+accessToken));
		String accountRequestBody  = "";
		
		try {
			accountRequestBody = new String(Files.readAllBytes(Paths.get("account_request_permissions.txt")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String accountUrl = SandBoxConstants.constants.getProperty("accountRequest");
		
		
		String response = HTTPSClient.callPostMethod(client, accountUrl, headers, accountRequestBody);
		
		
		
		
		
		return response;
		
	}
	
	

}
