package utility;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.security.interfaces.RSAPrivateKey;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.PlainHeader;
import com.nimbusds.jose.crypto.RSASSASigner;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.PlainJWT;
import com.nimbusds.jwt.SignedJWT;

import constants.SandBoxConstants;
import pojo.AccessToken;





public class TokenUtility {
				
	


	public static String getSignJwt(String issuerId,  JWTClaimsSet jwtClaimsSet)  {
				
		JWK jwk = null;		
		SignedJWT signedJWT = null;
		try {

			String signingPrivateKey = SandBoxConstants.constants.getProperty("singingPrivateKey");
			String cert = SandBoxConstants.constants.getProperty("signingPublicCertificate");
			
			System.out.println(signingPrivateKey);
			System.out.println(cert);
			Key privateKey = getPrivateKey(signingPrivateKey);
			
			RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) privateKey;			
			RSAPublicKey rsaPublicKey = (RSAPublicKey) getPublicKey(cert);					
			
			jwk = new RSAKey.Builder(rsaPublicKey)
				    .privateKey( rsaPrivateKey)
				    .keyID(UUID.randomUUID().toString()) 
				    .build();
						
			JWSHeader jwsHeader = new JWSHeader
	                .Builder(JWSAlgorithm.parse("RS256"))
	                .type(JOSEObjectType.JWT)
	                .keyID(SandBoxConstants.constants.getProperty("keyId"))               
	                .build();
	        signedJWT = new SignedJWT(jwsHeader, jwtClaimsSet);
	        signedJWT.sign(new RSASSASigner((RSAKey) jwk));
			
		} catch (JOSEException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        return signedJWT.serialize();
       
    }

	

	 // read private key from a .key file
	 		 private static  PrivateKey getPrivateKey(String filename)
	 	   		   {
	 			 
	 	   			String privateKeyContent;
	 	   			PrivateKey privKey = null;
	 				try {
	 					privateKeyContent = new String(Files.readAllBytes(Paths.get(filename)));
	 					privateKeyContent = privateKeyContent.replaceAll("\\n", "").
	 		   					replace("-----BEGIN PRIVATE KEY-----", "").
	 		   					replace("-----END PRIVATE KEY-----", "");
	 		   			 KeyFactory kf = KeyFactory.getInstance("RSA");
	 		   			
	 		   			PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
	 		   	        privKey = kf.generatePrivate(keySpecPKCS8);
	 				} catch (IOException e) {
	 					// TODO Auto-generated catch block
	 					e.printStackTrace();
	 				} catch (NoSuchAlgorithmException e) {
	 					// TODO Auto-generated catch block
	 					e.printStackTrace();
	 				} catch (InvalidKeySpecException e) {
	 					// TODO Auto-generated catch block
	 					e.printStackTrace();
	 				}  			
	 	   			  			
	 	   			
	 	   	          	        	  	         	          	     	   
	 	   		   return privKey;
	 	   		   
	 	   }

    
   public static AccessToken getAccessToken(List<NameValuePair> form ) {
	   
	    AccessToken accessToken = null;
	    String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
	    String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
	    String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
	    String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
	    String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
		
		
		CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
				transportCertAlias, idStorePassword, trasportStorePassWord);
        String tokenUrl = SandBoxConstants.constants.getProperty("tokenUrl");
		String response = HTTPSClient.callPostMethod(client, tokenUrl, form);

	
		if (response != null && response.contains("access_token")) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				accessToken = mapper.readValue(response, AccessToken.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else {
			System.out.println(response.toString());
		}
		
	   return accessToken;
   }
    
   private static PublicKey getPublicKey(String fileName) {
	   PublicKey pub = null;
	   try {
		FileInputStream fi = new FileInputStream(fileName);
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		Certificate cert = cf.generateCertificate(fi);
		
		pub = cert.getPublicKey();
		
		
		
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CertificateException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   
	   
	   return pub;
   }
   
  
   
   public static  JWTClaimsSet getSandBoxClaims() {
	   	long current = System.currentTimeMillis() ;
	   	long exp = current + 360000;
	      JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
	      .issuer(SandBoxConstants.constants.getProperty("software_statement_id"))
	      .subject(SandBoxConstants.constants.getProperty("software_statement_id"))
	      .claim("scope", SandBoxConstants.constants.getProperty("clientScopes"))
	      .audience(SandBoxConstants.constants.getProperty("aud"))
	      .jwtID(UUID.randomUUID().toString())   
	      .issueTime(new Date(current)) 
	      .expirationTime(new Date(exp))                                          
	      .build();
	      
	      return jwtClaimsSet;

		}
   
   public static  JWTClaimsSet getDynamicRegistrationClaims(String ssaToken) {
	   String ssa = "";
	   List<String> response = new ArrayList<String>();
	   response.add("code");
	   response.add("code id_token");
	   
	   List<String> grant_token = new ArrayList<String>();
	   
	   grant_token.add("authorization_code");
	   grant_token.add("refresh_token");
	   grant_token.add("client_credentials");
	   
	   List<String> redirect_uris = new ArrayList<String>();
	   redirect_uris.add("https://app.getpostman.com/oauth2/callback");
	   

	   
	   List<String> token_end_point_array = new ArrayList<String>();
	   token_end_point_array.add("client_secret_basic");
	   token_end_point_array.add("client_secret_post");
	   token_end_point_array.add("client_secret_jwt");
	   token_end_point_array.add("private_key_jwt");
	   
	   String[] token_endpoint_array = new String[2];
	   token_endpoint_array[0] = "client_secret_basic";
	   token_endpoint_array[1] = "private_key_jwt";
	   	ssa = ssaToken;
	   	long current = System.currentTimeMillis() ;
	   	long exp = current + 7200000;
	      JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
	      .claim("token_endpoint_auth_signing_alg", "RS256")
	      .claim("grant_types", grant_token)
	      .claim("subject_type", "public")
	      .claim("application_type", "web")	      
	      .issuer(SandBoxConstants.constants.getProperty("software_statement_id"))
	      .claim("redirect_uris", redirect_uris)   
	      .claim("token_endpoint_auth_method", "client_secret_basic")
	   
	     
	      .claim("scope", "openid accounts")
	      .claim("request_object_signing_alg", "none")
	      .jwtID(UUID.randomUUID().toString())   
	      .issueTime(new Date(current)) 
	      .expirationTime(new Date(exp))  
	      .claim("response_types", response)
	      .claim("id_token_signed_response_alg", "RS256")
	      .claim("software_statement", ssa)
	      .audience("https://modelobankauth2018.o3bank.co.uk:4101")	                                              
	      .build();
	      
	      return jwtClaimsSet;

		}


      public static AccessToken getSandboxAccessToken(String softwareStatementID, String jwt) {
   	   
  	    AccessToken accessToken = null;
  	    String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
  	    String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
  	    String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
  	    String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
  	    String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
  		
  		
  		CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
  				transportCertAlias, idStorePassword, trasportStorePassWord);
         String tokenUrl = SandBoxConstants.constants.getProperty("sandboxTokenUrl");
         
       //Sandbox access token request body
       		List<NameValuePair> form = new ArrayList<NameValuePair>();
       		form.add(new BasicNameValuePair("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer"));
       		form.add(new BasicNameValuePair("client_id", softwareStatementID));
       		form.add(new BasicNameValuePair("client_assertion", jwt));
       		form.add(new BasicNameValuePair("scope", SandBoxConstants.constants.getProperty("clientScopes")));
       		form.add(new BasicNameValuePair("grant_type", "client_credentials"));
       		
       //Sandbox access token request header
       		 List<BasicHeader> headers = new ArrayList<BasicHeader> ();
       		 headers.add(new BasicHeader("Accept", "*/*"));
       		 headers.add(new BasicHeader("Content-Type", "application/x-www-form-urlencoded"));
       		 headers.add(new BasicHeader("Accept-Encoding", "gzip, deflate"));
       		
       
       	String response = HTTPSClient.postClient(client, tokenUrl, headers, form);

  	
  		if (response != null && response.contains("access_token")) {
  			ObjectMapper mapper = new ObjectMapper();
  			try {
  				accessToken = mapper.readValue(response, AccessToken.class);
  			} catch (IOException e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  			}
  			
  		} else {
  			System.out.println(response.toString());
  		}
  		
  	   return accessToken;
    }
      
      public static String getSSA(String accessToken, String softwareStatementId, String organisationId) {
    	  String ssaToken = "";
    	  String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
  	      String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
  	      String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
  	      String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
  	      String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
  		
  		
  		CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
  				transportCertAlias, idStorePassword, trasportStorePassWord); 		

  		String urlEndPoint = "https://matls-ssaapi.openbankingtest.org.uk/api/v1rc2/tpp/"
					+ organisationId + "/ssa/" + softwareStatementId;
  		
  		BasicHeader header = new BasicHeader("Authorization", "Bearer "+accessToken);
  		List<BasicHeader> headers = new ArrayList<BasicHeader>();
  		headers.add(header);
  		
  		ssaToken = HTTPSClient.callGetMethod(client, urlEndPoint, headers);   	  
  		return ssaToken;    	  
      }
      
      public static String getPreUrlJwt(String clientId, String accountId, 
				String redirectUri, String aud)  {			
			
			JWK jwk = null;
			PlainJWT signedJWT = null;
			String signingPrivateKey = SandBoxConstants.constants.getProperty("singingPrivateKey");
			String cert = SandBoxConstants.constants.getProperty("signingPublicCertificate");
			
			System.out.println(signingPrivateKey);
			System.out.println(cert);
			Key privateKey = getPrivateKey(signingPrivateKey);
			
			RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) privateKey;			
			RSAPublicKey rsaPublicKey = (RSAPublicKey) getPublicKey(cert);					
			
			jwk = new RSAKey.Builder(rsaPublicKey)
				    .privateKey( rsaPrivateKey)
				    .keyID(UUID.randomUUID().toString()) 
				    .build();						
			
			
			PlainHeader jwsHeader = new PlainHeader
			.Builder()	               	                             
			.build();
			
			
			JWTClaimsSet jwtClaimSet = getPreUrlJwtClaims(clientId, accountId, redirectUri, aud);
			signedJWT = new PlainJWT(jwsHeader, jwtClaimSet);
			//  signedJWT.sign(new RSASSASigner((RSAKey) jwk));
			return signedJWT.serialize();
			
			}
      
      
      
      private static JWTClaimsSet getPreUrlJwtClaims(String clientId, String accountId, 
				String redirectUri, String aud) {

			long current = System.currentTimeMillis() ;
			
			long exp = current + 7200000;
			String claims = "";
			JSONObject jsonObj = null;
			
		
			
			JSONObject claims_string = getPreUrlCaims(accountId);
			
			System.out.println(claims_string.toString());
			
			JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
			
			
						  .audience(aud)
						  .issuer(clientId)
						  .claim("scope", "accounts openid")
						  .claim("claims", claims_string.toMap())
						  .build();
			
			
			return jwtClaimsSet;
			
			}
      private static JSONObject getPreUrlCaims(String accountRequest) {
    	  
    	  JSONObject jsonObject = new JSONObject();
  		jsonObject.put("essential", true);
  		jsonObject.put("value", accountRequest);
  		
  		JSONObject jsonObject1 = new JSONObject();
  		jsonObject1.put("openbanking_intent_id", jsonObject );
  		

  		JSONObject idToken1 = new JSONObject();

  		idToken1.put("id_token", jsonObject1);
  		
  	//	userInfoJson.put("id_token", idToken);
  		
  		return idToken1;
    	  
      }
      
      public static String getBasicAuthToken(String client_id, String client_secret) {
    	  org.apache.commons.codec.binary.Base64  b = new org.apache.commons.codec.binary.Base64();
          String encoding = b.encodeAsString(new String(client_id+":"+client_secret).getBytes());       
          return encoding;
  	}
   
	
	
      
     
}